package esm.TES4Gecko;

import java.util.HashMap;

public final class FunctionCode
{
	public static final int CanHaveFlames = 153;

	public static final int CanPayCrimeGold = 127;

	public static final int GetActorValue = 14;

	public static final int GetAlarmed = 61;

	public static final int GetAmountSoldStolen = 190;

	public static final int GetAngle = 8;

	public static final int GetArmorRating = 81;

	public static final int GetArmorRatingUpperBody = 274;

	public static final int GetAttacked = 63;

	public static final int GetBarterGold = 264;

	public static final int GetBaseActorValue = 277;

	public static final int GetClassDefaultMatch = 229;

	public static final int GetClothingValue = 41;

	public static final int GetCrime = 122;

	public static final int GetCrimeGold = 116;

	public static final int GetCurrentAIPackage = 110;

	public static final int GetCurrentAIProcedure = 143;

	public static final int GetCurrentTime = 18;

	public static final int GetCurrentWeatherPercent = 148;

	public static final int GetDayOfWeek = 170;

	public static final int GetDead = 46;

	public static final int GetDeadCount = 84;

	public static final int GetDestroyed = 203;

	public static final int GetDetected = 45;

	public static final int GetDetectionLevel = 180;

	public static final int GetDisabled = 35;

	public static final int GetDisease = 39;

	public static final int GetDisposition = 76;

	public static final int GetDistance = 1;

	public static final int GetDoorDefaultOpen = 215;

	public static final int GetEquipped = 182;

	public static final int GetFactionRank = 73;

	public static final int GetFactionRankDifference = 60;

	public static final int GetFatiguePercentage = 128;

	public static final int GetFriendHit = 288;

	public static final int GetFurnitureMarkerID = 160;

	public static final int GetGlobalValue = 74;

	public static final int GetGold = 48;

	public static final int GetHeadingAngle = 99;

	public static final int GetIdleDoneOnce = 318;

	public static final int GetIgnoreFriendlyHits = 338;

	public static final int GetInCell = 67;

	public static final int GetInCellParam = 230;

	public static final int GetInFaction = 71;

	public static final int GetInSameCell = 32;

	public static final int GetInWorldspace = 310;

	public static final int GetInvestmentGold = 305;

	public static final int GetIsAlerted = 91;

	public static final int GetIsClass = 68;

	public static final int GetIsClassDefault = 228;

	public static final int GetIsCreature = 64;

	public static final int GetIsCurrentPackage = 161;

	public static final int GetIsCurrentWeather = 149;

	public static final int GetIsGhost = 237;

	public static final int GetIsID = 72;

	public static final int GetIsPlayableRace = 254;

	public static final int GetIsPlayerBirthsign = 224;

	public static final int GetIsRace = 69;

	public static final int GetIsReference = 136;

	public static final int GetIsSex = 70;

	public static final int GetIsUsedItem = 246;

	public static final int GetIsUsedItemType = 247;

	public static final int GetItemCount = 47;

	public static final int GetKnockedState = 107;

	public static final int GetLevel = 80;

	public static final int GetLineOfSight = 27;

	public static final int GetLockLevel = 65;

	public static final int GetLocked = 5;

	public static final int GetNoRumors = 320;

	public static final int GetOffersServicesNow = 255;

	public static final int GetOpenState = 157;

	public static final int GetPCExpelled = 193;

	public static final int GetPCFactionAttack = 199;

	public static final int GetPCFactionMurder = 195;

	public static final int GetPCFactionSteal = 197;

	public static final int GetPCFactionSubmitAuthority = 201;

	public static final int GetPCFame = 249;

	public static final int GetPCInFaction = 132;

	public static final int GetPCInfamy = 251;

	public static final int GetPCIsClass = 129;

	public static final int GetPCIsRace = 130;

	public static final int GetPCIsSex = 131;

	public static final int GetPCMiscStat = 312;

	public static final int GetPersuasionNumber = 225;

	public static final int GetPlayerControlsDisabled = 98;

	public static final int GetPlayerHasLastRiddenHorse = 362;

	public static final int GetPlayerInSEWorld = 365;

	public static final int GetPos = 6;

	public static final int GetQuestRunning = 56;

	public static final int GetQuestVariable = 79;

	public static final int GetRandomPercent = 77;

	public static final int GetRestrained = 244;

	public static final int GetScale = 24;

	public static final int GetScriptVariable = 53;

	public static final int GetSecondsPassed = 12;

	public static final int GetShouldAttack = 66;

	public static final int GetSitting = 159;

	public static final int GetSleeping = 49;

	public static final int GetStage = 58;

	public static final int GetStageDone = 59;

	public static final int GetStartingAngle = 11;

	public static final int GetStartingPos = 10;

	public static final int GetTalkedToPC = 50;

	public static final int GetTalkedToPCParam = 172;

	public static final int GetTimeDead = 361;

	public static final int GetTotalPersuasionNumber = 315;

	public static final int GetTrespassWarningLevel = 144;

	public static final int GetUnconscious = 242;

	public static final int GetUsedItemActivate = 259;

	public static final int GetUsedItemLevel = 258;

	public static final int GetVampire = 40;

	public static final int GetWalkSpeed = 142;

	public static final int GetWeaponAnimType = 108;

	public static final int GetWeaponSkillType = 109;

	public static final int GetWindSpeed = 147;

	public static final int HasFlames = 154;

	public static final int HasMagicEffect = 214;

	public static final int HasVampireFed = 227;

	public static final int IsActor = 353;

	public static final int IsActorAVictim = 314;

	public static final int IsActorEvil = 313;

	public static final int IsActorUsingATorch = 306;

	public static final int IsCellOwner = 280;

	public static final int IsCloudy = 267;

	public static final int IsContinuingPackagePCNear = 150;

	public static final int IsCurrentFurnitureObj = 163;

	public static final int IsCurrentFurnitureRef = 162;

	public static final int IsEssential = 354;

	public static final int IsFacingUp = 106;

	public static final int IsGuard = 125;

	public static final int IsHorseStolen = 282;

	public static final int IsIdlePlaying = 112;

	public static final int IsInCombat = 289;

	public static final int IsInDangerousWater = 332;

	public static final int IsInInterior = 300;

	public static final int IsInMyOwnedCell = 146;

	public static final int IsLeftUp = 285;

	public static final int IsOwner = 278;

	public static final int IsPCAMurderer = 176;

	public static final int IsPCSleeping = 175;

	public static final int IsPlayerInJail = 171;

	public static final int IsPlayerMovingIntoNewSpace = 358;

	public static final int IsPlayersLastRiddenHorse = 339;

	public static final int IsPleasant = 266;

	public static final int IsRaining = 62;

	public static final int IsRidingHorse = 327;

	public static final int IsRunning = 287;

	public static final int IsShieldOut = 103;

	public static final int IsSneaking = 286;

	public static final int IsSnowing = 75;

	public static final int IsSpellTarget = 223;

	public static final int IsSwimming = 185;

	public static final int IsTalking = 141;

	public static final int IsTimePassing = 265;

	public static final int IsTorchOut = 102;

	public static final int IsTrespassing = 145;

	public static final int IsTurnArrest = 329;

	public static final int IsWaiting = 111;

	public static final int IsWeaponOut = 101;

	public static final int IsXBox = 309;

	public static final int IsYielding = 104;

	public static final int MenuMode = 36;

	public static final int SameFaction = 42;

	public static final int SameFactionAsPC = 133;

	public static final int SameRace = 43;

	public static final int SameRaceAsPC = 134;

	public static final int SameSex = 44;

	public static final int SameSexAsPC = 135;

	public static final int WhichServiceMenu = 323;

	public static final HashMap<String, Integer> funcCodeMap = new HashMap<String, Integer>()
	{
	};

	public static final HashMap<Integer, String> funcCodeNameMap = new HashMap<Integer, String>()
	{
	};

	public static final String[] funcCodeList =
	{ "CanHaveFlames", "CanPayCrimeGold", "GetActorValue", "GetAlarmed", "GetAmountSoldStolen", "GetAngle", "GetArmorRating",
			"GetArmorRatingUpperBody", "GetAttacked", "GetBarterGold", "GetBaseActorValue", "GetClassDefaultMatch", "GetClothingValue",
			"GetCrime", "GetCrimeGold", "GetCurrentAIPackage", "GetCurrentAIProcedure", "GetCurrentTime", "GetCurrentWeatherPercent",
			"GetDayOfWeek", "GetDead", "GetDeadCount", "GetDestroyed", "GetDetected", "GetDetectionLevel", "GetDisabled", "GetDisease",
			"GetDisposition", "GetDistance", "GetDoorDefaultOpen", "GetEquipped", "GetFactionRank", "GetFactionRankDifference",
			"GetFatiguePercentage", "GetFriendHit", "GetFurnitureMarkerID", "GetGlobalValue", "GetGold", "GetHeadingAngle",
			"GetIdleDoneOnce", "GetIgnoreFriendlyHits", "GetInCell", "GetInCellParam", "GetInFaction", "GetInSameCell", "GetInWorldspace",
			"GetInvestmentGold", "GetIsAlerted", "GetIsClass", "GetIsClassDefault", "GetIsCreature", "GetIsCurrentPackage",
			"GetIsCurrentWeather", "GetIsGhost", "GetIsID", "GetIsPlayableRace", "GetIsPlayerBirthsign", "GetIsRace", "GetIsReference",
			"GetIsSex", "GetIsUsedItem", "GetIsUsedItemType", "GetItemCount", "GetKnockedState", "GetLevel", "GetLineOfSight",
			"GetLockLevel", "GetLocked", "GetNoRumors", "GetOffersServicesNow", "GetOpenState", "GetPCExpelled", "GetPCFactionAttack",
			"GetPCFactionMurder", "GetPCFactionSteal", "GetPCFactionSubmitAuthority", "GetPCFame", "GetPCInFaction", "GetPCInfamy",
			"GetPCIsClass", "GetPCIsRace", "GetPCIsSex", "GetPCMiscStat", "GetPersuasionNumber", "GetPlayerControlsDisabled",
			"GetPlayerHasLastRiddenHorse", "GetPlayerInSEWorld", "GetPos", "GetQuestRunning", "GetQuestVariable", "GetRandomPercent",
			"GetRestrained", "GetScale", "GetScriptVariable", "GetSecondsPassed", "GetShouldAttack", "GetSitting", "GetSleeping",
			"GetStage", "GetStageDone", "GetStartingAngle", "GetStartingPos", "GetTalkedToPC", "GetTalkedToPCParam", "GetTimeDead",
			"GetTotalPersuasionNumber", "GetTrespassWarningLevel", "GetUnconscious", "GetUsedItemActivate", "GetUsedItemLevel",
			"GetVampire", "GetWalkSpeed", "GetWeaponAnimType", "GetWeaponSkillType", "GetWindSpeed", "HasFlames", "HasMagicEffect",
			"HasVampireFed", "IsActor", "IsActorAVictim", "IsActorEvil", "IsActorUsingATorch", "IsCellOwner", "IsCloudy",
			"IsContinuingPackagePCNear", "IsCurrentFurnitureObj", "IsCurrentFurnitureRef", "IsEssential", "IsFacingUp", "IsGuard",
			"IsHorseStolen", "IsIdlePlaying", "IsInCombat", "IsInDangerousWater", "IsInInterior", "IsInMyOwnedCell", "IsLeftUp", "IsOwner",
			"IsPCAMurderer", "IsPCSleeping", "IsPlayerInJail", "IsPlayerMovingIntoNewSpace", "IsPlayersLastRiddenHorse", "IsPleasant",
			"IsRaining", "IsRidingHorse", "IsRunning", "IsShieldOut", "IsSneaking", "IsSnowing", "IsSpellTarget", "IsSwimming",
			"IsTalking", "IsTimePassing", "IsTorchOut", "IsTrespassing", "IsTurnArrest", "IsWaiting", "IsWeaponOut", "IsXBox",
			"IsYielding", "MenuMode", "SameFaction", "SameFactionAsPC", "SameRace", "SameRaceAsPC", "SameSex", "SameSexAsPC",
			"WhichServiceMenu" };

	public static String getFuncCodeName(int funcCodeType)
	{
		if (!funcCodeNameMap.containsKey(Integer.valueOf(funcCodeType)))
			return String.format("Unknown function [0x%03x hex, %d dec]", new Object[]
			{ Integer.valueOf(funcCodeType), Integer.valueOf(funcCodeType) });
		return funcCodeNameMap.get(Integer.valueOf(funcCodeType));
	}

	public static boolean isValid(int param)
	{
		return (param == 153) || (param == 127) || (param == 14) || (param == 61) || (param == 190) || (param == 8) || (param == 81)
				|| (param == 274) || (param == 63) || (param == 264) || (param == 277) || (param == 229) || (param == 41) || (param == 122)
				|| (param == 116) || (param == 110) || (param == 143) || (param == 18) || (param == 148) || (param == 170) || (param == 46)
				|| (param == 84) || (param == 203) || (param == 45) || (param == 180) || (param == 35) || (param == 39) || (param == 76)
				|| (param == 1) || (param == 215) || (param == 182) || (param == 73) || (param == 60) || (param == 128) || (param == 288)
				|| (param == 160) || (param == 74) || (param == 48) || (param == 99) || (param == 318) || (param == 338) || (param == 67)
				|| (param == 230) || (param == 71) || (param == 32) || (param == 310) || (param == 305) || (param == 91) || (param == 68)
				|| (param == 228) || (param == 64) || (param == 161) || (param == 149) || (param == 237) || (param == 72) || (param == 254)
				|| (param == 224) || (param == 69) || (param == 136) || (param == 70) || (param == 246) || (param == 247) || (param == 47)
				|| (param == 107) || (param == 80) || (param == 27) || (param == 65) || (param == 5) || (param == 320) || (param == 255)
				|| (param == 157) || (param == 193) || (param == 199) || (param == 195) || (param == 197) || (param == 201)
				|| (param == 249) || (param == 132) || (param == 251) || (param == 129) || (param == 130) || (param == 131)
				|| (param == 312) || (param == 225) || (param == 98) || (param == 362) || (param == 365) || (param == 6) || (param == 56)
				|| (param == 79) || (param == 77) || (param == 244) || (param == 24) || (param == 53) || (param == 12) || (param == 66)
				|| (param == 159) || (param == 49) || (param == 58) || (param == 59) || (param == 11) || (param == 10) || (param == 50)
				|| (param == 172) || (param == 361) || (param == 315) || (param == 144) || (param == 242) || (param == 259)
				|| (param == 258) || (param == 40) || (param == 142) || (param == 108) || (param == 109) || (param == 147)
				|| (param == 154) || (param == 214) || (param == 227) || (param == 353) || (param == 314) || (param == 313)
				|| (param == 306) || (param == 280) || (param == 267) || (param == 150) || (param == 163) || (param == 162)
				|| (param == 354) || (param == 106) || (param == 125) || (param == 282) || (param == 112) || (param == 289)
				|| (param == 332) || (param == 300) || (param == 146) || (param == 285) || (param == 278) || (param == 176)
				|| (param == 175) || (param == 171) || (param == 358) || (param == 339) || (param == 266) || (param == 62)
				|| (param == 327) || (param == 287) || (param == 103) || (param == 286) || (param == 75) || (param == 223)
				|| (param == 185) || (param == 141) || (param == 265) || (param == 102) || (param == 145) || (param == 329)
				|| (param == 111) || (param == 101) || (param == 309) || (param == 104) || (param == 36) || (param == 42) || (param == 133)
				|| (param == 43) || (param == 134) || (param == 44) || (param == 135) || (param == 323);
	}
}

/* Location:           C:\temp\TES4Gecko\
 * Qualified Name:     TES4Gecko.FunctionCode
 * JD-Core Version:    0.6.0
 */